import React, { Component } from 'react';

class Table extends Component {
    render() {
        const prmData = this.props.data;
        if (prmData) {
            return (
                <tr>
                    <td>{prmData.country_code}</td>
                    <td>{prmData.country_name}</td>
                    <td>
                        <button className="my-button btn-yellow" onClick={() => this.props.update(prmData)}>Edit</button>
                        <button className="my-button btn-red" onClick={() => this.props.remove(prmData.id)}>Delete</button>
                    </td>
                </tr>
            );
        } else {
            return (
                <tr>
                    <td colSpan="5">
                        <h1>No Data</h1>
                    </td>
                </tr>
            );
        }
    }
}

export default Table;
