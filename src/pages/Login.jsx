import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import axios from 'axios';

function Login() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [validation, setValidation] = useState([]);
    const [newPassword, setNewPassword] = useState("");
    const [confirmNewPassword, setConfirmNewPassword] = useState("");
    const [changePasswordValidation, setChangePasswordValidation] = useState([]);
    const [showChangePasswordForm, setShowChangePasswordForm] = useState(false); // State untuk mengontrol tampilan form perubahan password
    const history = useHistory();

    useEffect(() => {
        if (localStorage.getItem('token')) {
            history.push('/read');
        }
    }, [history]);

    const loginHandler = async (e) => {
        e.preventDefault();
        const confirmed = window.confirm('Are you sure you want to log in?');
        
        if (confirmed) {
            const formData = new FormData();
            formData.append('email', email);
            formData.append('password', password);

            try {
                const response = await axios.post('http://backend-dev.cakra-tech.co.id/api/login', formData);

                localStorage.setItem('token', response.data.token);
                history.push('/read');
            } catch (error) {
                setValidation(error.response.data);
            }
        }
    };

    const handlePasswordChange = async (e) => {
        e.preventDefault();

        if (newPassword !== confirmNewPassword) {
            setChangePasswordValidation({ message: "New passwords do not match." });
            return;
        }

        try {
            const response = await axios.post('http://backend-dev.cakra-tech.co.id/api/change-password', {
                email,
                newPassword,
            });

            setChangePasswordValidation({ message: "Password changed successfully." });
            setNewPassword("");
            setConfirmNewPassword("");
        } catch (error) {
            setChangePasswordValidation(error.response.data);
        }
    };

    return (
        <div className="container" style={{ borderRadius: '20px', backgroundColor: 'blue' , boxSizing: 'borderBox',
            boxShadow: 'rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px' }}>
            <div className="row justify-content-center"  style={{  backgroundColor: 'blue', width: '100%'}}>
                <div className="col-md-4"   style={{  backgroundColor: 'blue', width: '100%'}}>
                    <div className="card border-0 rounded shadow-sm "  style={{  backgroundColor: 'blue', width: '100%'}}>
                        <div className="card-body" style={{  backgroundColor: 'blue', width: '100%'}}>
                            <h4 className="fw-bold"  style={{  backgroundColor: 'blue', width: '100%', color: 'white'}}>HALAMAN LOGIN</h4>
                            <hr/>
                            {/* Tampilkan form perubahan password jika showChangePasswordForm bernilai true */}
                            {showChangePasswordForm ? (
                                <form onSubmit={handlePasswordChange}>
                                    <div className="mb-3">
                                        <label className="form-label">New Password</label>
                                        <input type="password" className="form-control" value={newPassword} onChange={(e) => setNewPassword(e.target.value)} />
                                    </div>
                                    <div className="mb-3">
                                        <label className="form-label">Confirm New Password</label>
                                        <input type="password" className="form-control" value={confirmNewPassword} onChange={(e) => setConfirmNewPassword(e.target.value)} />
                                    </div>
                                    {changePasswordValidation.message && (
                                        <div className="alert alert-danger">
                                            {changePasswordValidation.message}
                                        </div>
                                    )}
                                    <button type="submit" className="btn btn-primary">Change Password</button>
                                </form>
                            ) : (
                                <form onSubmit={loginHandler}>
                                    <div className="mb-3">
                                        <label className="form-label" style={{    marginTop: '30px',backgroundColor: 'blue', width: '100%', color: 'white'}}>ALAMAT EMAIL</label>
                                        <input type="email" className="form-control" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Masukkan Alamat Email"/>
                                    </div>
                                    {validation.email && (
                                        <div className="alert alert-danger">
                                            {validation.email[0]}
                                        </div>
                                    )}
                                    <div className="mb-3">
                                        <label className="form-label" style={{   marginTop: '10px', backgroundColor: 'blue', width: '100%', color: 'white'}}>PASSWORD</label>
                                        <input type="password" className="form-control" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Masukkan Password"/>
                                    </div>
                                    {validation.password && (
                                        <div className="alert alert-danger">
                                            {validation.password[0]}
                                        </div>
                                    )}
                                    
                                    <div className="d-grid gap-2">
                                        <button type="submit" className="btn btn-primary" style={{  marginTop: '30px', width: '100%', color: 'white'}}>LOGIN</button>
                                        {/* Toggle state showChangePasswordForm saat tombol di klik */}
                                        <p style={{ marginTop: '10px', color: 'white', textAlign: 'center' }}>
                                            Belum punya akun? <Link to="/register" style={{ color: 'white', textDecoration: 'underline' }}>Daftar di sini</Link>
                                            {' | '}
                                            <span 
                                                style={{ color: 'white', textDecoration: 'underline', cursor: 'pointer' }}
                                                onClick={() => setShowChangePasswordForm(!showChangePasswordForm)}
                                            >
                                                Change Password
                                            </span>
                                        </p>
                                    </div>
                                </form>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;
