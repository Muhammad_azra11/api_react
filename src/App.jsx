import React from 'react';
// import './App.css';
import { Switch, Route } from "react-router-dom";
import Register from './pages/Register';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import Create from './components/create';
import Read from './components/read';
import Update from './components/update';

function App() {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/create" component={Create} />
        <Route exact path="/read" component={Read} />
        <Route exact path="/update" component={Update} />
      </Switch>
    </div>
  );
}

export default App;