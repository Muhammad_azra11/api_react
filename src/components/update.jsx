import React, { useEffect, useState } from 'react';
import { Button, Checkbox, Form } from 'semantic-ui-react';
import axios from 'axios';
import { useHistory } from 'react-router';
// import { useNavigate } from 'react-router';

function Update() {
    const [countryCode, setCountryCode] = useState('');
    const [countryName, setCountryName] = useState('');
    const [checkbox, setCheckbox] = useState(false);
    const [id, setID] = useState(null);
    let history = useHistory();

    useEffect(() => {
        setID(localStorage.getItem('ID'));
        setCountryCode(localStorage.getItem('Country Code'));
        setCountryName(localStorage.getItem('Country Name'));
        setCheckbox(localStorage.getItem('Checkbox Value'));
    }, []);

    const updateAPIData = () => {
        const confirmed = window.confirm('Are you sure you want to update this data?');
        if (confirmed) {
            axios.put(`http://localhost:3005/country/${id}`, {
                countryCode,
                countryName,
                checkbox
            }).then(() => {
                history.push('/read');
            });
        }
    };

    return (
        <div>
            <Form className="create-form" style={{border: '1px solid black' ,padding: '50px',backgroundColor: "#4169E1", color: 'white', borderRadius: '10px'}}>
                <Form.Field>
                    <label style={{color: 'white'}}>Country Code</label>
                    <input placeholder='Country Code' value={countryCode} onChange={(e) => setCountryCode(e.target.value)}/>
                </Form.Field>
                <Form.Field>
                    <label style={{color: 'white'}}>Country Name</label>
                    <input placeholder='Country Name' value={countryName} onChange={(e) => setCountryName(e.target.value)}/>
                </Form.Field>
                <Form.Field>
                    <Checkbox style={{color: 'white'}} label='I agree to the Terms and Conditions' checked={checkbox} onChange={(e) => setCheckbox(!checkbox)}/>
                </Form.Field>
                <Button type='button' onClick={updateAPIData} style={{backgroundColor: "#87CEFA", color: '#ffff'}}>Update</Button> {/* Change type to 'button' */}
            </Form>
        </div>
    );
}

export default Update;
