import React, { useState } from 'react';
import { Button, Checkbox, Form } from 'semantic-ui-react';
import axios from 'axios';
import { useHistory } from 'react-router';

export default function Create() {
    const [countryCode, setCountryCode] = useState('');
    const [countryName, setCountryName] = useState('');
    const [checkbox, setCheckbox] = useState(false);
    const navigate = useHistory();

    const postData = () => {
        const confirmed = window.confirm('Are you sure you want to create this data?');
        if (confirmed) {
            axios.post(`http://localhost:3005/country/`, {
                countryCode,
                countryName,
                checkbox
            }).then(() => {
                navigate.push('/read');
            });
        }
    };

    return (
        <div>
            <Form className="create-form" style={{border: '1px solid black', padding: '55px', backgroundColor: '#009900' ,borderRadius: '20px'}}>
                <Form.Field>
                    <label style={{color:'white'}}>Country Code</label>
                    <input placeholder='Country Code' onChange={(e) => setCountryCode(e.target.value)} />
                </Form.Field>
                <Form.Field>
                    <label style={{color:'white'}}>Country Name</label>
                    <input placeholder='Country Name' onChange={(e) => setCountryName(e.target.value)} />
                </Form.Field>
                <Form.Field>
                    <Checkbox style={{color:'white'}} label='I agree to the Terms and Conditions' onChange={(e) => setCheckbox(!checkbox)} />
                </Form.Field>
                <Button onClick={postData} type='button'>Submit</Button> {/* Change type to 'button' to prevent default form submission */}
            </Form>
        </div>
    );
}
