import React, { useEffect, useState } from 'react';
import { Table, Button } from 'semantic-ui-react'; // Mengimpor Button juga
import axios from 'axios';
import './read.css';
import { Link, useHistory } from 'react-router-dom';

function Read() {
    const [APIData, setAPIData] = useState([]);
    const history = useHistory();

    useEffect(() => {
        axios.get(`http://localhost:3000/country`)
        .then((response) => {
            setAPIData(response.data);
        });
    }, []);

    const setData = (data) => {
        let { id, countryCode, countryName, checkbox } = data;
        localStorage.setItem('ID', id);
        localStorage.setItem('Country Code', countryCode);
        localStorage.setItem('Country Name', countryName);
        localStorage.setItem('Checkbox Value', checkbox)
     }

     
     const getData = () => {
         axios.get(` http://localhost:3005/country`)
         .then((getData) => {
             setAPIData(getData.data);
            })
        }


        const onDelete = (id) => {
            const confirmed = window.confirm('Are you sure you want to delete this data?');
            if (confirmed) {
                axios.delete(` http://localhost:3005/country${id}`)
                .then(() => {
                    // Refresh data after deletion
                    axios.get(` http://localhost:3005/country`)
                    .then((response) => {
                        setAPIData(response.data);
                    });
                });
            }
        }

        const logout = () => {
            const confirmed = window.confirm('Are you sure you want to logout?');
            if (confirmed) {
                localStorage.removeItem('token'); // Hapus token dari localStorage
                history.push('/'); // Redirect ke halaman login setelah logout
            }
        }

    return (
        <div>
             <Table.Cell>
                    <Link to='/create'> {/* Gunakan Link */}
                        <Button style={{backgroundColor: "#228B22", color: 'white'}}>Create</Button>
                    </Link>
             </Table.Cell>
            <Table singleLine style={{ padding: "60px", backgroundColor: '#D3D3D3', boxSizing: 'borderBox',
         boxShadow: 'rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px'}}>
                <Table.Header >
                    <Table.Row>
                        <Table.HeaderCell>Country Code</Table.HeaderCell>
                        <Table.HeaderCell>Country Name</Table.HeaderCell>
                        <Table.HeaderCell>Checkbox</Table.HeaderCell>
                        <Table.HeaderCell>Update</Table.HeaderCell>
                        <Table.HeaderCell>Delete</Table.HeaderCell> {/* Tambahkan kolom "Update" */}
                    </Table.Row>
                </Table.Header>

                <Table.Body >
                {APIData.map((data) => (
                    <Table.Row key={data.countryCode}>
                        <Table.Cell>{data.countryCode}</Table.Cell>
                        <Table.Cell>{data.countryName}</Table.Cell>
                        <Table.Cell>{data.checkbox ? 'Checked' : 'Unchecked'}</Table.Cell>
                        <Table.Cell>
                            <Link to='/update'> {/* Gunakan Link */}
                                <Button  onClick={() => setData(data)} style={{backgroundColor: "#32CD32", color: 'white'}}>Update</Button>
                            </Link>
                        </Table.Cell>
                          <Table.Cell>
                          <Button onClick={() => onDelete(data.id)} style={{backgroundColor: "red", color: 'white'}}>Delete</Button>
                         </Table.Cell>
                   </Table.Row>
                ))}
                </Table.Body>
            </Table>
            <Button onClick={logout} style={{ marginBottom: '20px', backgroundColor: "	#4169E1" , color: 'white'}}>Logout</Button> 
        </div>
    );
}


export default Read