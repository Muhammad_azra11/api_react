import React from 'react';
import { createRoot } from 'react-dom/client';
import { BrowserRouter, Route, Switch } from 'react-router-dom'; // Pastikan impor yang sesuai dari react-router-dom
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'semantic-ui-css/semantic.min.css';
// import './index.css';

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);
root.render(
  <BrowserRouter>
    <React.StrictMode>
      <Switch>
        <Route path="/" component={App} />
        {/* Tambahkan rute lain di sini */}
      </Switch>
    </React.StrictMode>
  </BrowserRouter>
);
