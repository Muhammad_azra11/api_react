// src/api.js
import React from 'react'; // Jika digunakan dalam komponen React

const BASE_URL = 'http://backend-dev.cakra-tech.co.id/api'; // Ganti dengan URL API Anda

export async function getItems() {
  const response = await fetch(`${BASE_URL}/country`);
  const data = await response.json();
  return data;
}

export async function createItem(itemData) {
  const response = await fetch(`${BASE_URL}/country`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(itemData),
  });
  const data = await response.json();
  return data;
}

export async function updateItem(id, itemData) {
  const response = await fetch(`${BASE_URL}/country/${id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(itemData),
  });
  const data = await response.json();
  return data;
}

export async function deleteItem(itemId) {
  const response = await fetch(`${BASE_URL}/country/${itemId}`, {
    method: 'DELETE',
  });
  const data = await response.json();
  return data;
}
